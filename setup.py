'''Install the crc_common package.'''

from setuptools import setup

setup(
    name='crc_common',
    version='0.3',
    description='Common core modules for CRC services',
    author='Craig Ciccone',
    author_email='crc888@gmail.com',
    url='https://www.crc-web.cf/',
    packages=['crc_common'],
    install_requires=[
        'Flask==0.12.2',
        'Jinja2==2.9.6',
        'requests==2.13.0',
        'statsd==3.2.1'
    ]
)
