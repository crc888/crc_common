'''
Statsd Flask extension to utilize Statsd via the application factory pattern.
'''

from flask import current_app
import statsd

# TODO: redo this statsd extension based on the Flask link in the docstring

class Statsd(object):
    '''
    Custom Statsd extension for Flask.
    
    http://flask.pocoo.org/docs/0.12/extensiondev/
    '''

    __client = None

    def __init__(self, app=None):
        '''
            Initialize the extension.
            
            Args:
                app (flask.Flask): The Flask application object.
        '''
        
        if app is not None:
            self.init_app(app)

    def init_app(self, app):
        app.config.setdefault('STATSD_HOST', 'localhost')
        app.config.setdefault('STATSD_PORT', 9125)
        app.config.setdefault('STATSD_PREFIX', None)
        app.config.setdefault('STATSD_RATE', 1)

    def timer(self, stat, rate=None):
        return self._client.timer(
            rate=rate or current_app.config['STATSD_RATE'],
            stat=stat
        )

    def timing(self, stat, delta, rate=None):
        return self._client.timing(
            rate=rate or current_app.config['STATSD_RATE'],
            delta=delta,
            stat=stat
        )

    def incr(self, stat, count=1, rate=None):
        return self._client.incr(
            rate=rate or current_app.config['STATSD_RATE'],
            count=count,
            stat=stat
        )

    def decr(self, stat, count=1, rate=None):
        return self.incr(stat, -count, rate)

    @property
    def _client(self):
        if not self.__client:
            self.__client = statsd.StatsClient(
                current_app.config['STATSD_HOST'],
                current_app.config['STATSD_PORT'],
                current_app.config['STATSD_PREFIX']
            )
        return self.__client
