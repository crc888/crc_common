'''Helper functions related to API calls.'''

import requests
from flask import abort, current_app as app
from requests.exceptions import ConnectionError, Timeout

from crc_common.custom_except import InvalidUsage


def api_call(api, **kwargs):
    '''
    Generic function to call / validate the HTTP status code from an API call.
    
    The function returns the JSON payload from the API call.
    Calling functions are expected to handle 200, 401, 403, and 404 responses.
    Each of those responses are expected to return a JSON payload.
    If the request fails (no connection, no response) a 500 error is raised.
    A 500 error is also raised if there is no JSON payload for valid HTTP codes.
    
    Args:
        api (str): The name of the api to be called.
        **kwargs: Arbitrary keyword arguments that will be passed to the API 
            as a JSON request.
    
    Returns:
        dict: The JSON respone from the API parsed into a python dictionary.
    '''

    # Format the URL for the API
    address = app.config['SERVICE_APIS_INT'][api]['address']
    path = app.config['SERVICE_APIS_INT'][api]['path']
    url = '{address:s}{path:s}'.format(
        address=address, path=path
    )

    # Call the API via the requests library
    try:
        if kwargs:
            req = requests.post(url=url, json=kwargs, timeout=10)
        else:
            req = requests.post(url=url, timeout=10)
    except Timeout:
        app.logger.error(
            'Timeout occurred communicating with API "{api:s}" @ "{url:s}"'.format(
                api=api,
                url=url
            )
        )
        abort(500)
    except ConnectionError:
        app.logger.error(
            'Failed to connect to API "{api:s}" @ "{url:s}"'.format(
                api=api,
                url=url
            )
        )
        abort(500)


    # Ensure a valid HTTP status code came back from the API call
    if req.status_code not in [200, 401, 403, 404]:
        app.logger.error(
            'HTTP status code of "{http_code:d}" returned from "{api:s}" @ "{url:s}"'.format(
                api=api,
                http_code=req.status_code,
                url=url
            )
        )
        abort(500)

    # Ensure a JSON payload was included with the response
    try:
        req.json()
    except ValueError:
        app.logger.error(
            'No JSON payload returned by API "{api:s}" @ "{url:s}"'.format(
                api=api,
                url=url
            )
        )
        abort(500)

    # TODO: possibly add back schema validation HERE (with unit testing)

    # Return the response as a dictionary
    return req.json()


def validate_request(json, schema, api):
    '''
    Initial validation of an API request
    
    Args:
        json (str): The JSON request object to be validated.
        schema (str): The JSON schema to validate the supplied JSOn against.
        api (str): The name of the API this JSON request was sent to. This is
            only used for logging purposes.
    
    Returns:
        dict: The validated JSON request data parsed into a python dictionary.
    
    Raises:
        InvalidUsage: If the JSON is missing or does not satisfy the schema.
    '''

    # Ensure a JSON payload is provided
    if not json:
        raise InvalidUsage(
            api=api, status='failure', message='JSON payload is missing'
        )

    # Validate the request format
    req = schema().load(json)
    if len(req.errors) > 0:
        raise InvalidUsage(
            api=api, status='failure', message='Invalid JSON payload format', reasons=req.errors
        )

    # Return the request as a dictionary
    return req.data
