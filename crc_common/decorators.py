'''This module is used to help manage user roles.'''

from functools import wraps

from flask import flash, g, render_template

from crc_common.custom_except import InvalidUsage

# Conditionally import uWSGI for production to postfork the scheduler
# This is to ensure the scheduler runs on every uWSGI worker instance
try:
    from uwsgidecorators import postfork
except ImportError:
    pass


class uwsgi_postfork():
    '''
    Conditionally decorate a function with the uWSGI postfork decorator.
    
    Attributes:
        condition (bool): The boolean condition evaluated to determine if 
            the uWSGI postfork decorator should be applied to the function.
    '''

    def __init__(self, condition):
        self.condition = condition

    def __call__(self, func):
        '''
        The method executed when this decorator is called.
        
        Returns:
            The postfork decorated function if the condition is true, otherwise 
                the function is returned without the postfork decorator applied.
        '''

        # Return the function unchanged (not decorated)
        if not self.condition:
            return func

        # Return the function decorated with uWSGI postfork
        return postfork(func)


def required_roles(*roles):
    '''
    Decorator function to determine if a user has the required roles.
    
    This is used to restrict Flask views to user with certain roles.
    
    Args:
        *roles: Variable length argument list of user roles.
        
    Returns:
        The function decorated by this decorator if the user has all roles
            passed into *roles. Otherwise it returns an error template page.
    '''

    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if set(roles).issubset(g.user.roles):
                return f(*args, **kwargs)
            else:
                flash(
                    'You do not have permission to access that resource',
                    category='warning'
                )
                return render_template('error.html', err_req='Access Denied')
        return wrapped
    return wrapper


def requires_token(request):
    '''
    Decorator function to determine if a request has the required auth bearer 
    token.
    
    Args:
        request (flask.Request): The Flask request object.
        
    Returns:
        The function decorated by this decorator if the request has the token.

    Raises:
        InvalidUsage: If the request does not have the required token.
    '''

    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            header = request.headers.get('Authorization')
            if header:
                if 'Bearer' in header:
                    token = header.split('Bearer ')[1]
                    if token:
                        return f(*args, **kwargs)
            raise InvalidUsage(
                api=request.endpoint, status='failure', message='Bearer header with valid token required'
            )
        return wrapped
    return wrapper


def reg_dd(path, role, dropdown, header, title):
    '''
    Decorator for Flask views that register dynamic HTML dropdowns.
    
    This decorator sets meta data for Flask view functions to allow them
    to be dynamically populated in front end dropdown menus in the navbar.
    
    Args:
        path (str): The context path of the URL for this dropdown link.
        role (str): The role required to see this dropdown link (all, user, or 
            admin).
        dropdown (str): The name of the dropdown this view will live under.
        header (str): The heading inside the dropdown this view will live 
            under.
        title (str): The text displayed to the user for this link. It should 
            be a short name representing where the link takes the user.
    
    Returns: The wrapped function with all argument parameters added as meta 
        data.
    '''

    def wrapped(fn):
        @wraps(fn)
        def wrapped_f(*args, **kwargs):
            return fn(*args, **kwargs)
        wrapped_f._reg_dd = True
        wrapped_f._reg_path = path
        wrapped_f._reg_role = role
        wrapped_f._reg_dropdown = dropdown
        wrapped_f._reg_header = header
        wrapped_f._reg_title = title
        return wrapped_f
    return wrapped


def reg_link(path, role, title):
    '''
    Decorator for Flask views that register dynamic HTML links.
    
    This decorator sets meta data for Flask view functions to allow them
    to be dynamically populated in front end links in the navbar.
    
    Args:
        path (str): The context path of the URL for this link.
        role (str): The role required to see this link (all, user, or admin).
        title (str): The text displayed to the user for this link. It should 
            be a short name representing where the link takes the user.
    
    Returns: The wrapped function with all argument parameters added as meta 
        data.
    '''

    def wrapped(fn):
        @wraps(fn)
        def wrapped_f(*args, **kwargs):
            return fn(*args, **kwargs)
        wrapped_f._reg_link = True
        wrapped_f._reg_path = path
        wrapped_f._reg_role = role
        wrapped_f._reg_title = title
        return wrapped_f
    return wrapped


def reg_api_ext(path, name):
    '''
    Decorator for Flask views that register external APIs.
    
    This decorator sets meta data for Flask view functions to allow them
    to dynamically register external APIs in the service registry.
    
    Args:
        path (str): The context path of the URL for this API.
        name (str): A unique identifier that the application will use to 
            lookup this API when it needs to call it.
    
    Returns: The wrapped function with all argument parameters added as meta 
        data.
    '''

    def wrapped(fn):
        @wraps(fn)
        def wrapped_f(*args, **kwargs):
            return fn(*args, **kwargs)
        wrapped_f._reg_api_ext = True
        wrapped_f._reg_name = name
        wrapped_f._reg_path = path
        return wrapped_f
    return wrapped


def reg_api_int(path, name):
    '''
    Decorator for Flask views that register internal APIs.
    
    This decorator sets meta data for Flask view functions to allow them
    to dynamically register internal APIs in the service registry.
    
    Args:
        path (str): The context path of the URL for this API.
        name (str): A unique identifier that the application will use to 
            lookup this API when it needs to call it.
    
    Returns: The wrapped function with all argument parameters added as meta 
        data.
    '''

    def wrapped(fn):
        @wraps(fn)
        def wrapped_f(*args, **kwargs):
            return fn(*args, **kwargs)
        wrapped_f._reg_api_int = True
        wrapped_f._reg_name = name
        wrapped_f._reg_path = path
        return wrapped_f
    return wrapped
