'''Utility for safely redirecting URL requests'''

from urllib.parse import urlparse, urljoin

from flask import request

# TODO: look back into this safe_redirect function

def is_safe_url(target):
    '''
    Method to check if the next parameter is safe to redirect to
    
    Args:
        target (str): The target URL path.
    
    Returns:
        str: A sanitized target URL path.
    '''

    ref_url = urlparse(request.host_url)
    test_url = urlparse(urljoin(request.host_url, target))

    return test_url.scheme in ('http', 'https') and \
        ref_url.netloc == test_url.netloc
