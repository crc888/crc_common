'''
Common functions to create a Flask app via the application factory pattern.
'''

import json
from datetime import datetime
from importlib import import_module
from logging import Formatter
from logging.handlers import RotatingFileHandler
from os.path import join
from socket import gethostname

import requests
from jinja2 import ChoiceLoader, PackageLoader

from crc_common.decorators import uwsgi_postfork


def apply_config(app, default_conf, config=None):
    '''
    Apply the default config and any override config from conf/config.py.
    
    Args:
        app (flask.Flask): The Flask application object.
        default_conf (object): The default / production configuration object.
            See conf/config.py.
        config (object): An optional config to override the default config.
            See connf/config.py for available config options.
    '''

    # Apply the default (production) configurations
    app.config.from_object(default_conf)

    # Override the configurations if necessary
    if config:
        import_module('conf.config')
        app.config.from_object(config)


def configure_blueprints(app, blueprints):
    '''
    Import views to register all Flask blueprints.
    
    Args:
        app (flask.Flask): The Flask application object.
        blueprints (:obj:`list` of :obj:`flask.Blueprint`): A list of all 
            Flask blueprints used by the application.
    '''

    # Import all views and register all Blueprints
    for blueprint in blueprints:
        import_module(blueprint.import_name)
        app.register_blueprint(blueprint)


def configure_filters(app):
    '''
    Set custom Jinja2 template filters.
    
    Args:
        app (flask.Flask): The Flask application object.
    '''

    @app.template_filter('convert_dt')
    def convert_dt(dt):
        '''
        Parse datetime strings in 2 possible formats
        
        Args:
            dt (str): The datetime string to parse into a datetime object.
        
        Returns:
            datetime: The datetime object parsed from the string.
        '''

        # Initialize the date to an empty string
        date = ''

        # Try to parse the date between the two possible formats
        try:
            date = datetime.strptime(dt, '%Y-%m-%dT%H:%M:%S.%f')
        except ValueError:
            date = datetime.strptime(dt, '%a, %d %b %Y %H:%M:%S %Z')

        # Return the parsed date
        return date


def configure_logging(app):
    '''
    Configure file based logging
    
    Args:
        app (flask.Flask): The Flask application object.
    '''

    # Set the log file name
    log_file = join(
        app.config['LOG_DIR'],
        '{app:s}.{host:s}.log'.format(
            app=app.name,
            host=gethostname()
        )
    )

    # Set a rotating file handler
    file_handler = RotatingFileHandler(
        filename=log_file,
        mode='a',
        maxBytes=app.config['LOG_SIZE'],
        backupCount=app.config['LOG_COUNT']
    )

    # Set the log format
    formatter = Formatter(app.config['LOG_FORMAT'])
    file_handler.setFormatter(formatter)

    # Set the logging level
    app.logger.setLevel(app.config['LOG_LEVEL'])

    # Add the rotating file handler to the Flask logger
    app.logger.addHandler(file_handler)


def extend_jinja_loader(app):
    '''
    Extend the Flask app's jinja2 loader to include common templates.
    
    Args:
        app (flask.Flask): The Flask application object.
    '''

    loader = ChoiceLoader(
        [app.jinja_loader, PackageLoader('crc_common_fe', 'templates')]
    )
    app.jinja_loader = loader


def register_service(app):
    '''
    Register the application service via consul.
    
    This is done through the consul registration API defined in the Flask
    config variable CONSUL_REGISTER_URL. The context path for this consul API
    should be "v1/agent/service/register".
    
    Args:
        app (flask.Flask): The Flask application object.
    
    Returns:
        payload (dict): A python dictionary representation of the JSON payload
            that was sent to consul to register the service.
    
    '''

    # Initialize dicts that make up the tags
    apis_ext = {}
    apis_int = {}
    dropdowns = {'admin': {}, 'user': {}, 'all': {}}
    links = {'admin': [], 'user': [], 'all': []}

    # Loop through all application routes
    for route in app.view_functions:

        # Store each route function
        rt = app.view_functions[route]

        # Only register dropdowns for decorated routes
        if '_reg_dd' in rt.__dict__:

            # Formulate the JSON structure defining the dropdowns tag
            # Add the role and header if not already defined
            if rt._reg_dropdown not in dropdowns[rt._reg_role]:
                dropdowns[rt._reg_role][rt._reg_dropdown] = {}
            if rt._reg_header not in dropdowns[rt._reg_role][rt._reg_dropdown]:
                dropdowns[rt._reg_role][rt._reg_dropdown][rt._reg_header] = []
            dropdowns[rt._reg_role][rt._reg_dropdown][rt._reg_header].append(
                {
                    'address': app.config['SERVICE_ADDRESS'],
                    'path': rt._reg_path,
                    'title': rt._reg_title
                }
            )

        # Only register links for decorated routes
        if '_reg_link' in rt.__dict__:

            # Formulate the JSON structure defining the links tag
            links[rt._reg_role].append(
                {
                    'address': app.config['SERVICE_ADDRESS'],
                    'path': rt._reg_path,
                    'title': rt._reg_title
                }
            )

        # Only register external APIs for decorated routes
        if '_reg_api_ext' in rt.__dict__:

            # Formulate the JSON structure defining the external APIs tag
            apis_ext[rt._reg_name] = {
                'address': app.config['SERVICE_ADDRESS'],
                'path': rt._reg_path
            }

        # Only register internal APIs for decorated routes
        if '_reg_api_int' in rt.__dict__:

            # Formulate the JSON structure defining the internal APIs tag
            apis_int[rt._reg_name] = {
                'address': app.config['SERVICE_ADDRESS'],
                'path': rt._reg_path
            }

    # Formulate the payload expected by consul
    payload = {
        "Name": app.config['APP_NAME'],
        "Tags": [
            json.dumps({'apis_ext': apis_ext}),
            json.dumps({'apis_int': apis_int}),
            json.dumps({'dropdowns': dropdowns}),
            json.dumps({'links': links})
        ],
        "Address": app.config['SERVICE_ADDRESS'],
        "Port": app.config['APP_PORT'],
        "Check": {
            "DeregisterCriticalServiceAfter": app.config['DEREGISTER_INTERVAL'],
            "HTTP": app.config['SERVICE_HEALTH_CHECK_ROUTE'],
            "Interval": app.config['SERVICE_CHECK_INTERVAL'],
            "Timeout": app.config['SERVICE_TIMEOUT']
        }
    }

    # Register the service with consul
    requests.put(app.config['CONSUL_REGISTER_URL'], json=payload)
    
    # Return the payload used to register the service in consul
    return payload


def postfork_scheduler(app, scheduler, worker):
    '''
    Initialize the APScheduler instance using uWSGI postfork for production.
    
    Args:
        app (flask.Flask): The Flask application object.
        scheduler (BackgroundScheduler): The APScheduler object.
        worker (bool): A flag denotating if this app is a celery worker or not.
    '''

    # Determine how to run the scheduler
    # It must utilize uWSGI postfork for production
    @uwsgi_postfork(app.config['UWSGI_POSTFORK'] and not worker)
    def init_scheduler():
        '''Add the service update job to the scheduler and start it.'''

        # Configure and run the scheduler
        sched_serv_updates(app, scheduler)
        scheduler.start()

        # Log the initialization of the scheduler
        app.logger.debug('Scheduler started')

    # Call init_scheduler here for non production use
    # It will never be called by uWSGI postfork
    if not app.config['UWSGI_POSTFORK'] or worker:
        init_scheduler()


def sched_serv_updates(app, scheduler):
    '''
    Schedule an APScheduler job to refresh the consul service data.
    
    Args:
        app (flask.Flask): The Flask application object.
        scheduler (BackgroundScheduler): The APScheduler object.
    '''

    # Add a job to schedule service endpoint updates
    scheduler.add_job(
        id='service_update',
        name='service_update',
        func=set_service_endpoints,
        args=[app],
        replace_existing=True,
        trigger='interval',
        seconds=app.config['SERVICE_UPDATE_MIN']
    )


def set_service_endpoints(app):
    '''
    Discover all service endpoints registered in consul.

    This is done through the consul service catalog API defined in the Flask
    config variable CONSUL_SERVICES_URL. The context path for this consul API
    should be "/v1/catalog/services".
    
    Args:
        app (flask.Flask): The Flask application object.
    '''

    # Get the service details from consul
    r = requests.get(app.config['CONSUL_SERVICES_URL'])
    services = r.json()

    # Initialize dicts that come from the tags
    apis_ext = {}
    apis_int = {}
    dropdowns = {'admin': {}, 'user': {}, 'all': {}}
    links = {'admin': [], 'user': [], 'all': []}

    # Loop through all services returned from the consul registry
    for service in services:

        # Ignore the console default service
        if service == 'consul':
            continue

        # Initialize temporary storage for tags
        tmp_apis_ext = {}
        tmp_apis_int = {}
        tmp_dropdowns = {}
        tmp_links = {}

        # Load the JSON stored in the tag parameters of the service
        # Note that list order is not guaranteed
        tags = [json.loads(tag) for tag in services[service]]
        for tag in tags:
            if 'apis_ext' in tag:
                tmp_apis_ext = tag['apis_ext']
            elif 'apis_int' in tag:
                tmp_apis_int = tag['apis_int']
            elif 'dropdowns' in tag:
                tmp_dropdowns = tag['dropdowns']
            elif 'links' in tag:
                tmp_links = tag['links']

        # Store the external APIs for each service
        for api in tmp_apis_ext:
            apis_ext[api] = tmp_apis_ext[api]

        # Store the internal APIs for each service
        for api in tmp_apis_int:
            apis_int[api] = tmp_apis_int[api]

        # Store the dropdowns for each service
        # TODO: try to make this code look better
        for access in tmp_dropdowns:
            for dropdown in tmp_dropdowns[access]:
                if dropdown not in dropdowns[access]:
                    dropdowns[access][dropdown] = {}
                for header in tmp_dropdowns[access][dropdown]:
                    if header not in dropdowns[access][dropdown]:
                        dropdowns[access][dropdown][header] = []
                    dropdowns[access][dropdown][header] = dropdowns[access][dropdown][header] + \
                        tmp_dropdowns[access][dropdown][header]

        # Store the links for each service
        for access in tmp_links:
            links[access] = links[access] + tmp_links[access]

    # Set the Flask config only if it changed
    if 'SERVICE_APIS_INT' not in app.config or app.config['SERVICE_APIS_INT'] != apis_int:
        app.config['SERVICE_APIS_INT'] = apis_int
    if 'SERVICE_APIS_EXT' not in app.config or app.config['SERVICE_APIS_EXT'] != apis_ext:
        app.config['SERVICE_APIS_EXT'] = apis_ext
    if 'SERVICE_DROPDOWNS' not in app.config or app.config['SERVICE_DROPDOWNS'] != dropdowns:
        app.config['SERVICE_DROPDOWNS'] = dropdowns
    if 'SERVICE_LINKS' not in app.config or app.config['SERVICE_LINKS'] != dropdowns:
        app.config['SERVICE_LINKS'] = links
