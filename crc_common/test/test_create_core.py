'''Unit tests for the create_core module.'''

import json
import unittest
from unittest.mock import patch

from flask import Flask

from crc_common.create_core import register_service, set_service_endpoints
from crc_common.decorators import reg_dd, reg_link, reg_api_ext, reg_api_int

# Global test data
DD_PTH = '/dd'
L_PTH = '/link'
API_EXT_PTH = '/api_ext'
API_INT_PTH = '/api_int'
USR_ROLE = 'user'
ALL_ROLE = 'all'
DD = 'Features'
HEAD = 'Header'
TITLE = 'Title'
L_TITLE = 'Link Title'
EXT_NAME = 'external-api'
INT_NAME = 'internal-api'
DEREG_INT = '30m'
PORT = 5000
HEALTH_CHECK = 'http://localhost:5000/health'
CHECK_INT = '5m'
TIMEOUT = '20s'
NAME = 'crc-test-app'
CONSUL_REG_URL = 'http://localhost:8500/v1/agent/service/register'
CONSUL_SERV_URL = 'http://localhost:8500/v1/catalog/services'
ADDR = 'http://localhost:5000'


class TestCreateCore(unittest.TestCase):
    '''Class containing all tests for core Flask app creation functions'''

    def setUp(self):
        '''
        Create a Flask object for all tests. Add routes to this flask app
        that are to be used for testing. Set the Flask config variables as
        needed for testing as well.
        '''
        
        app = Flask(__name__)

        @app.route(DD_PTH, methods=['GET'])
        @reg_dd(path=DD_PTH, role=USR_ROLE, dropdown=DD, header=HEAD, title=TITLE)
        def dd():
            return DD_PTH

        @app.route(L_PTH, methods=['GET'])
        @reg_link(path=L_PTH, role=ALL_ROLE, title=L_TITLE)
        def link():
            return L_PTH

        @app.route(API_EXT_PTH, methods=['GET'])
        @reg_api_ext(path=API_EXT_PTH, name=EXT_NAME)
        def api_ext():
            return API_EXT_PTH

        @app.route(API_INT_PTH, methods=['GET'])
        @reg_api_int(path=API_INT_PTH, name=INT_NAME)
        def api_int():
            return API_INT_PTH

        self.app = app
        self.app.config['SERVICE_ADDRESS'] = ADDR
        self.app.config['APP_NAME'] = NAME
        self.app.config['APP_PORT'] = PORT
        self.app.config['DEREGISTER_INTERVAL'] = DEREG_INT
        self.app.config['SERVICE_HEALTH_CHECK_ROUTE'] = HEALTH_CHECK
        self.app.config['SERVICE_CHECK_INTERVAL'] = CHECK_INT
        self.app.config['SERVICE_TIMEOUT'] = TIMEOUT
        self.app.config['CONSUL_REGISTER_URL'] = CONSUL_REG_URL
        self.app.config['CONSUL_SERVICES_URL'] = CONSUL_SERV_URL

    @patch('crc_common.create_core.requests')
    def test_service_registration(self, mock_req):
        '''
        Validate the tags created during service registration and validate 
        the tags returned by the service registry.
        
        Args:
            mock_req (unittest.mock.MagicMock): Mock the requests the module
                called inside crc_common.create_core.
        '''

        # Mock the request to register a service in Consul
        mock_req.put.return_value = True

        # Call the register_service function
        payload = register_service(self.app)
        tags = payload['Tags']
        parsed_tags = [json.loads(tag) for tag in tags]

        # Parse the tags
        for parsed_tag in parsed_tags:
            if 'apis_ext' in parsed_tag:
                tag_apis_ext = parsed_tag['apis_ext']
            elif 'apis_int' in parsed_tag:
                tag_apis_int = parsed_tag['apis_int']
            elif 'dropdowns' in parsed_tag:
                tag_dropdowns = parsed_tag['dropdowns']
            elif 'links' in parsed_tag:
                tag_links = parsed_tag['links']

        # Validate the api_ext tags during registration
        assert EXT_NAME in tag_apis_ext
        assert tag_apis_ext[EXT_NAME]['address'] == ADDR
        assert tag_apis_ext[EXT_NAME]['path'] == API_EXT_PTH

        # Validate the api_int tags during registration
        assert INT_NAME in tag_apis_int
        assert tag_apis_int[INT_NAME]['address'] == ADDR
        assert tag_apis_int[INT_NAME]['path'] == API_INT_PTH

        # Validate the dropdown tags during registration
        assert len(tag_dropdowns['user']) == 1
        assert len(tag_dropdowns['user'][DD][HEAD]) == 1
        assert tag_dropdowns['user'][DD][HEAD][0]['address'] == ADDR
        assert tag_dropdowns['user'][DD][HEAD][0]['path'] == DD_PTH
        assert tag_dropdowns['user'][DD][HEAD][0]['title'] == TITLE

        # Validate the link tags during registration
        assert len(tag_links['all']) == 1
        assert tag_links['all'][0]['address'] == ADDR
        assert tag_links['all'][0]['path'] == L_PTH
        assert tag_links['all'][0]['title'] == L_TITLE

        # Mock the request to get service data from Consul
        # Reuse tags from earlier as its the same data structure
        class MockReq():
            def json(self):
                return {'crc-service': tags}
        mock_req.get.return_value = MockReq()

        # Call the set_service_endpoints function
        set_service_endpoints(self.app)

        # Validate the api_ext tags after setting service endpoints
        assert self.app.config['SERVICE_APIS_EXT'][EXT_NAME]['address'] == ADDR
        assert self.app.config['SERVICE_APIS_EXT'][EXT_NAME]['path'] == API_EXT_PTH

        # Validate the api_int tags after setting service endpoints
        assert self.app.config['SERVICE_APIS_INT'][INT_NAME]['address'] == ADDR
        assert self.app.config['SERVICE_APIS_INT'][INT_NAME]['path'] == API_INT_PTH

        # Validate the dropdown tags after setting service endpoints
        assert len(self.app.config['SERVICE_DROPDOWNS']['user']) == 1
        assert len(self.app.config['SERVICE_DROPDOWNS']['user'][DD][HEAD]) == 1
        assert self.app.config['SERVICE_DROPDOWNS']['user'][DD][HEAD][0]['address'] == ADDR
        assert self.app.config['SERVICE_DROPDOWNS']['user'][DD][HEAD][0]['path'] == DD_PTH
        assert self.app.config['SERVICE_DROPDOWNS']['user'][DD][HEAD][0]['title'] == TITLE

        # Validate the link tags after setting service endpoints
        assert len(self.app.config['SERVICE_LINKS']['all']) == 1
        assert self.app.config['SERVICE_LINKS']['all'][0]['address'] == ADDR
        assert self.app.config['SERVICE_LINKS']['all'][0]['path'] == L_PTH
        assert self.app.config['SERVICE_LINKS']['all'][0]['title'] == L_TITLE


if __name__ == '__main__':
    unittest.main()
