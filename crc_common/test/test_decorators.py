'''Unit tests for decorators'''

# Standard library imports
import unittest
from unittest.mock import patch

# Third party imports
from flask import Flask, g

# Local imports
from crc_common.decorators import required_roles

# Global test data
ALLOW_MSG = 'this is the message when allowed'
BLOCK_MSG = 'this is the message when blocked'


class TestRequiredRoles(unittest.TestCase):
    '''Class containing all tests for the required_roles decorator'''

    def setUp(self):

        self.app = Flask(__name__)

    @patch('crc_common.decorators.render_template')
    @patch('crc_common.decorators.flash')
    def test_one_role_fail(self, mock_flash, mock_rt):
        '''
        Test that it blocks users without the required role.
        
        Args:
            mock_flash (unittest.mock.MagicMock): Mock Flask flash called 
                inside crc_common.decorators.
            mock_rt (unittest.mock.MagicMock): Mock Flask render_template
                inside crc_common.decorators.
        '''

        # Work within the Flask application context
        with self.app.app_context():

            # Mock the user
            class MockUser():
                roles = ['block']
            g.user = MockUser()

            # Mock flash and render_template
            mock_flash.return_value = None
            mock_rt.return_value = BLOCK_MSG

            # Created a function to test the decorator
            @required_roles('allow')
            def foo():
                return ALLOW_MSG

            # Call the decorated function
            assert foo() == BLOCK_MSG

    @patch('crc_common.decorators.render_template')
    @patch('crc_common.decorators.flash')
    def test_one_role_pass(self, mock_flash, mock_rt):
        '''
        Test that it allows users with the required role
        
        Args:
            mock_flash (unittest.mock.MagicMock): Mock Flask flash called 
                inside crc_common.decorators.
            mock_rt (unittest.mock.MagicMock): Mock Flask render_template
                inside crc_common.decorators.
        '''

        # Work within the Flask application context
        with self.app.app_context():

            # Mock the user
            class MockUser():
                roles = ['allow']
            g.user = MockUser()

            # Mock flash and render_template
            mock_flash.return_value = None
            mock_rt.return_value = BLOCK_MSG

            # Created a function to test the decorator
            @required_roles('allow')
            def foo():
                return ALLOW_MSG

            # Call the decorated function
            assert foo() == ALLOW_MSG

    @patch('crc_common.decorators.render_template')
    @patch('crc_common.decorators.flash')
    def test_multi_role_fail(self, mock_flash, mock_rt):
        '''
        Test that it blocks users without the required roles
        
        Args:
            mock_flash (unittest.mock.MagicMock): Mock Flask flash called 
                inside crc_common.decorators.
            mock_rt (unittest.mock.MagicMock): Mock Flask render_template
                inside crc_common.decorators.
        '''

        # Work within the Flask application context
        with self.app.app_context():

            # Mock the user
            class MockUser():
                roles = ['allow1', 'block2']
            g.user = MockUser()

            # Mock flash and render_template
            mock_flash.return_value = None
            mock_rt.return_value = BLOCK_MSG

            # Created a function to test the decorator
            @required_roles('allow1', 'allow2')
            def foo():
                return ALLOW_MSG

            # Call the decorated function
            assert foo() == BLOCK_MSG

    @patch('crc_common.decorators.render_template')
    @patch('crc_common.decorators.flash')
    def test_multi_role_pass(self, mock_flash, mock_rt):
        '''
        Test that it allows users with the required roles
        
        Args:
            mock_flash (unittest.mock.MagicMock): Mock Flask flash called 
                inside crc_common.decorators.
            mock_rt (unittest.mock.MagicMock): Mock Flask render_template
                inside crc_common.decorators.
        '''

        # Work within the Flask application context
        with self.app.app_context():

            # Mock the user
            class MockUser():
                roles = ['allow1', 'allow2']
            g.user = MockUser()

            # Mock flash and render_template
            mock_flash.return_value = None
            mock_rt.return_value = BLOCK_MSG

            # Created a function to test the decorator
            @required_roles('allow1', 'allow2')
            def foo():
                return ALLOW_MSG

            # Call the decorated function
            assert foo() == ALLOW_MSG


if __name__ == '__main__':
    unittest.main()
