'''Unit tests for the api module'''

import unittest
from unittest.mock import patch

from flask import Flask
from requests.exceptions import ConnectionError, Timeout
from werkzeug.exceptions import InternalServerError

from crc_common.api import api_call

# Global test data
API = {'test-api': {'address': 'http://fake.com', 'path': '/not/real'}}
JSON_DICT = {'valid': 'json'}


class TestApiCall(unittest.TestCase):
    '''Class containing all tests for the api_call function'''

    def setUp(self):
        '''Create a Flask object for all tests.'''

        self.app = Flask(__name__)
        self.app.config['SERVICE_APIS_INT'] = API

    @patch('crc_common.api.requests')
    def test_connect_error(self, mock_req):
        '''
        Test api_call when a connection error occurs. Expect that an 
        InternalServerError is raised.
        
        Args:
            mock_req (unittest.mock.MagicMock): Mock the requests the module
                called inside crc_common.api.
        '''

        # Mock the request exception
        mock_req.post.side_effect = ConnectionError
        mock_req.exceptions.ConnectionError = ConnectionError

        # Call the test API ensuring an InternalServerError is raised
        with self.app.app_context():
            with self.assertRaises(InternalServerError):
                api_call('test-api')

    @patch('crc_common.api.requests')
    def test_timeout(self, mock_req):
        '''
        Test api_call when a timeout occurs. Expect that an InternalServerError
        is raised.
        
        Args:
            mock_req (unittest.mock.MagicMock): Mock the requests the module
                called inside crc_common.api.
        '''

        # Mock the request exception
        mock_req.post.side_effect = Timeout
        mock_req.exceptions.Timeout = Timeout

        # Call the test API ensuring an InternalServerError is raised
        with self.app.app_context():
            with self.assertRaises(InternalServerError):
                api_call('test-api')

    @patch('crc_common.api.requests')
    def test_status(self, mock_req):
        '''
        Test api_call when an invalid status code is returned. Expect that an 
        InternalServerError is raised.
        
        Args:
            mock_req (unittest.mock.MagicMock): Mock the requests the module
                called inside crc_common.api.
        '''

        # Mock the request
        class MockReq():
            status_code = 500
        mock_req.post.return_value = MockReq()

        # Call the test API ensuring an InternalServerError is raised
        with self.app.app_context():
            with self.assertRaises(InternalServerError):
                api_call('test-api')

    @patch('crc_common.api.requests')
    def test_payload(self, mock_req):
        '''
        Test api_call when no JSON payload is returned. Expect that an 
        InternalServerError is raised.
        
        Args:
            mock_req (unittest.mock.MagicMock): Mock the requests the module
                called inside crc_common.api.
        '''

        # Mock the request
        class MockReq():
            status_code = 200

            def json(self):
                raise ValueError 

        mock_req.post.return_value = MockReq()

        # Call the test API ensuring an InternalServerError is raised
        with self.app.app_context():
            with self.assertRaises(InternalServerError):
                api_call('test-api')

    @patch('crc_common.api.requests')
    def test_valid(self, mock_req):
        '''
        Test api_call when an valid response is returned. A valid dictionary
        should be returned by the function.
        
        Args:
            mock_req (unittest.mock.MagicMock): Mock the requests the module
                called inside crc_common.api.
        '''

        # Mock the request
        class MockReq():
            status_code = 200

            def json(self):
                return JSON_DICT
        mock_req.post.return_value = MockReq()

        # Call the test API ensuring a valid result is returned
        with self.app.app_context():
            result = api_call('test-api')
            assert result == JSON_DICT


if __name__ == '__main__':
    unittest.main()
