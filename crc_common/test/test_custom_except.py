'''Unit tests for the custom_except module'''

# Standard library imports
import unittest

# Local imports
from crc_common.custom_except import InvalidUsage

# Global test data
API = 'api'
STATUS = 'failure'
MSG = 'error occurred'
ERR_REASON = 'error reason'
PAYLOAD = 'payload'


class TestCustomExcept(unittest.TestCase):
    '''Class containing all tests for custom exceptions'''

    def test_req_except_parms(self):
        '''Ensure all required exception params are functional'''

        try:
            raise InvalidUsage(api=API, status=STATUS, message=MSG)
        except InvalidUsage as e:
            assert e.api == API
            assert e.status == STATUS
            assert e.message == MSG
            assert e.status_code == 400

    def test_req_except_all_parms(self):
        '''Ensure all exception params are functional'''

        try:
            raise InvalidUsage(
                api='api',
                status='failure',
                message='error occurred',
                reasons=ERR_REASON,
                status_code=500,
                payload=PAYLOAD
            )
        except InvalidUsage as e:
            assert e.api == API
            assert e.status == STATUS
            assert e.message == MSG
            assert e.status_code == 500
            assert e.reasons == ERR_REASON
            assert e.payload == PAYLOAD


if __name__ == '__main__':
    unittest.main()
