'''Custom exceptions are defined here.'''


class InvalidUsage(Exception):
    '''
    Custom exception for invalid API requests.
    
    Attributes:
        api (str): The API being called when this exception is raised.
        status (str): The status of the API call, i.e. failure.
        message (str): A brief description of why the exception was raised.
        reasons (:obj:`str`, optional): Optional detailed information as to 
            why the exception was raised.
        status_code (str): The desired HTTP return code to be sent back.
        paylod (:obj:`str`, optional): Optional payload to be sent back.
    '''

    def __init__(self, api, status, message, reasons=None, status_code=400, payload=None):
        Exception.__init__(self)
        self.api = api
        self.status = status
        self.message = message
        self.reasons = reasons
        self.status_code = status_code
        self.payload = payload

    def to_dict(self):
        rv = dict(self.payload or ())
        rv['api'] = self.api
        rv['status'] = self.status
        rv['message'] = self.message
        rv['reasons'] = self.reasons
        return rv
